package mx.azkane.grafos;

import io.reactivex.Observable;
import io.reactivex.rxjavafx.observables.JavaFxObservable;
import io.reactivex.rxjavafx.sources.Flag;
import io.reactivex.subjects.PublishSubject;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.util.Pair;
import mx.azkane.grafos.controls.Flecha;
import mx.azkane.grafos.controls.Nodo;
import mx.azkane.grafos.modelo.CaminoCorto;
import mx.azkane.grafos.modelo.Grafo;
import mx.azkane.grafos.modelo.Vertice;
import mx.azkane.grafos.serialization.EdgeSerializable;
import mx.azkane.grafos.serialization.GrafoSerializable;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GraphView {
    private final MenuItem    addNodo             = new MenuItem("Agregar Vertice");
    private final ContextMenu basePaneContextMenu = new ContextMenu(addNodo);

    private HashSet<String> idsNodos = new HashSet<>();
    private Observable<Nodo> nodosAgregadoObs;
    private Observable<Nodo> nodoSeleccionado;
    private Observable<Nodo> enlaceNodoReqObs;
    private Observable<Nodo> nodoFuente;
    private Observable<Nodo> nodoDestino;

    @FXML
    private Pane                         basePane;
    private Observable<Double>           distanciaShortestPath;
    private Observable<CaminoCorto>      ultimoShortestPath;
    private Observable<Optional<Flecha>> nodoEnlazadoObs;
    private Observable<Boolean>          nodoDeshabilitado;
    private Set<Flecha>                  paintedArrows;
    private Observable<HashSet<Vertice>> nodos;
    private PublishSubject<Boolean> desactivaVerticesObs           = PublishSubject.create();
    private boolean                 lastRunDesactivarVerticeStatus = false;

    @FXML
    private void initialize() {
        nodosAgregadoObs = JavaFxObservable.additionsOf(basePane.getChildren())
                                           .filter(Nodo.class::isInstance)
                                           .map(nodo -> ((Nodo) nodo))
                                           .publish()
                                           .refCount();

        JavaFxObservable.additionsOf(basePane.getChildren())
                        .filter(Flecha.class::isInstance)
                        .subscribe(Node::toBack);

        nodoSeleccionado = nodosAgregadoObs.map(Nodo::getSelectedObservable)
                                           .flatMap(nodos -> nodos);
        enlaceNodoReqObs = nodosAgregadoObs.map(Nodo::getEnlazarObservable)
                                           .flatMap(nodos -> nodos);

        nodoFuente = nodosAgregadoObs.map(Nodo::getNodoFuenteObservable)
                                     .flatMap(nodos -> nodos);

        nodoDestino = nodosAgregadoObs.map(Nodo::getNodoDestinoObservable)
                                      .flatMap(nodos -> nodos);

        nodoDeshabilitado = nodosAgregadoObs.map(Nodo::getDeshabilitarNodoObserver)
                                            .flatMap(nodos -> nodos);

        nodosAgregadoObs.map(Nodo::getDeleteNodoObserver)
                        .flatMap(nodos -> nodos)
                        .subscribe(n -> {
                            Set<Flecha> flechas = n.delete();
                            basePane.getChildren()
                                    .removeAll(flechas);
                            basePane.getChildren()
                                    .remove(n);
                        });

        nodos = JavaFxObservable.changesOf(basePane.getChildren())
                                .filter(nodeEv -> Nodo.class.isInstance(nodeEv.getValue()))
                                .scan(new HashSet<Vertice>(),
                                      (acc, nodoEvent) -> {
                                          if (nodoEvent.getFlag() == Flag.ADDED) {
                                              acc.add(((Nodo) nodoEvent.getValue())
                                                              .getVertice());
                                          } else if (nodoEvent.getFlag() == Flag.REMOVED) {
                                              acc.remove(((Nodo) nodoEvent.getValue()).getVertice());
                                          }
                                          return acc;
                                      });

        handleSeleccionNodo();
        handleEnlaceNodos();
        handleCaminoCorto();

        initBaseContextMenuStream();
    }

    private void handleCaminoCorto() {

        ultimoShortestPath =
                Observable.combineLatest(nodoFuente,
                                         nodoDestino,
                                         nodos.startWith(new HashSet<Vertice>()),
                                         nodoEnlazadoObs.startWith(Optional.empty()),
                                         nodoDeshabilitado.startWith(false),
                                         desactivaVerticesObs,
                                         (fuente,
                                          destino,
                                          currentNodos,
                                          enlaceTrigger,
                                          deshabiltadoTrigger,
                                          verticesDesactivados) -> {
                                             System.out.println("Triggered!");
//                                             if (verticesDesactivados && !lastRunDesactivarVerticeStatus) {
                                             if (verticesDesactivados) {
                                                 System.out.println("vertices desactivados");
                                                 if (lastRunDesactivarVerticeStatus) {
                                                     activaNodos();
                                                 }
                                                 desactivaNodosRandom();
                                                 lastRunDesactivarVerticeStatus = true;
                                             } else {
                                                 System.out.println("vertices activados");
                                                 if (lastRunDesactivarVerticeStatus) {
                                                     activaNodos();
                                                     lastRunDesactivarVerticeStatus = false;
                                                 }
                                             }

                                             if (paintedArrows != null) {
                                                 paintedArrows.forEach(f -> {
                                                     if (!f.getStroke()
                                                           .equals(Paint.valueOf("GRAY"))) {
                                                         f.setStroke(Paint.valueOf("BLACK"));
                                                     }
                                                 });
                                             }
                                             Grafo grafo = new Grafo();
                                             grafo.setVertices(currentNodos);
                                             return Grafo.distanciaMinimaDesdeOrigen(
                                                     grafo,
                                                     fuente.getVertice(),
                                                     destino.getVertice());
                                         })
                          .share();
        ultimoShortestPath
                .doOnNext(System.out::println)
                .subscribe(caminoCorto -> {
                    List<Vertice> camino = caminoCorto.getCaminoCorto();
                    camino.add(caminoCorto.getVerticeDestino());

                    paintedArrows = basePane.getChildren()
                                            .stream()
                                            .filter(Nodo.class::isInstance)
                                            .map(n -> ((Nodo) n).maybeDrawPath(camino))
                                            .filter(Objects::nonNull)
                                            .collect(Collectors.toSet());

                    for (int i = 0; i < camino.size() - 1; i++) {
                        Vertice vertice     = camino.get(i);
                        Vertice nextVertice = camino.get(i + 1);
                        System.out.println(vertice.getNombre() + " -> " + nextVertice.getNombre());
                    }
                });

        distanciaShortestPath = ultimoShortestPath.map(CaminoCorto::getDistancia);
    }

    private void desactivaNodosRandom() {
        basePane.getChildren()
                .stream()
                .filter(n -> Math.random() > 0.9)
                .filter(Nodo.class::isInstance)
                .forEach(n ->
                                 ((Nodo) n).setDeshabilitado());
    }

    private void activaNodos() {
        basePane.getChildren()
                .stream()
                .filter(Nodo.class::isInstance)
                .forEach(n -> ((Nodo) n).setHabilitadoStatus());
    }

    private void initBaseContextMenuStream() {
        JavaFxObservable.eventsOf(basePane, MouseEvent.MOUSE_CLICKED)
                        .filter(me -> me.getTarget()
                                        .equals(basePane))
                        .subscribe(me -> {
                            if (me.getButton() == MouseButton.SECONDARY) {
                                basePaneContextMenu.show(basePane, me.getScreenX(), me.getScreenY());
                            } else {
                                basePaneContextMenu.hide();
                            }
                        });

        Observable<MouseEvent> mousePositionObservable = JavaFxObservable.eventsOf(basePane, MouseEvent.MOUSE_MOVED);

        Observable<Pair<MouseEvent, String>> nuevoNodoObservable =
                JavaFxObservable.actionEventsOf(addNodo)
                                .withLatestFrom(mousePositionObservable, (ae, me) -> me)
                                .flatMap(me -> JavaFxObservable
                                                 .fromDialog(makeInputDialog())
                                                 .toObservable(),
                                         Pair::new)
                                .publish()
                                .refCount();
        nuevoNodoObservable
                .filter(p -> idsNodos.contains(p.getValue()))
                .subscribe(p -> {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Oops");
                    alert.setContentText("Ya existe un nodo con etiqueta: '" + p.getValue() + '\'');
                    alert.show();
                });

        nuevoNodoObservable
                .filter(p -> !idsNodos.contains(p.getValue()))
                .subscribe(p -> {
                    basePane.getChildren()
                            .add(Nodo.of(new Vertice(p.getValue()),
                                         p.getKey()
                                          .getSceneX(),
                                         p.getKey()
                                          .getSceneY(),
                                         nodosAgregadoObs));
                    idsNodos.add(p.getValue());
                });
    }


    private void handleSeleccionNodo() {
        nodoSeleccionado
                .buffer(2, 1)
                .subscribe(l -> {
                    if (l.size() == 1) {
                        l.get(0)
                         .setFocus();
                    } else {
                        l.get(0)
                         .unsetFocus();
                        l.get(1)
                         .setFocus();
                    }
                });
    }

    private void handleEnlaceNodos() {
        nodoEnlazadoObs = enlaceNodoReqObs
                .withLatestFrom(nodoSeleccionado,
                                (nodoReq, nodoSel) ->
                                        JavaFxObservable.fromDialog(makeSizeDialog())
                                                        .toObservable()
                                                        .map(Double::parseDouble)
                                                        .map(dist -> nodoSel.addNodoAdyacente(nodoReq, dist)))
                .flatMap(optL -> optL)
                .publish()
                .refCount();

        // TODO: Handle Double::parseDouble InvalidNumber, catch exception on subscription and show warning
        nodoEnlazadoObs
                .subscribe(optL -> optL.map(l -> basePane.getChildren()
                                                         .add(l)));
    }

    public GrafoSerializable dumpState() {
        List<Nodo> state = this.basePane.getChildren()
                                        .stream()
                                        .filter(Nodo.class::isInstance)
                                        .map(n -> (Nodo) n)
                                        .collect(Collectors.toList());

        return GrafoSerializable.of(state);
    }

    public boolean loadState(GrafoSerializable grafo) {
        Map<String, Nodo> nodos = grafo.getNodos()
                                       .stream()
                                       .map(ns -> Nodo.of(new Vertice(ns.getNombre()),
                                                          ns.getPositionX(),
                                                          ns.getPositionY(),
                                                          nodosAgregadoObs))
                                       .collect(Collectors.toMap(Nodo::getNombre,
                                                                 Function.identity()));

        idsNodos.addAll(nodos.keySet());
        basePane.getChildren()
                .addAll(nodos.values());

        basePane.applyCss();
        basePane.layout();

        Set<EdgeSerializable> edges = grafo.getEdges();

        for (EdgeSerializable edge : edges) {
            Nodo origen  = nodos.get(edge.getOrigen());
            Nodo destino = nodos.get(edge.getDestino());
            if (origen == null || destino == null) {
                System.out.println("No se encontraron origen/destino: " + edge.getOrigen() + ":" + edge.getDestino());
                return false;
            }

            Optional<Flecha> flecha = origen.addNodoAdyacente(destino, edge.getDistance());
            flecha.ifPresent(basePane.getChildren()::add);
        }

        return true;
    }

    public Observable<Nodo> getNodoFuente() {
        return nodoFuente;
    }

    public Observable<Nodo> getNodoDestino() {
        return nodoDestino;
    }

    public Observable<Double> getDistanciaShortestPath() {
        return distanciaShortestPath;
    }

    public Observable<CaminoCorto> getUltimoShortestPath() {
        return ultimoShortestPath;
    }

    private TextInputDialog makeInputDialog() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Crear nodo");
        dialog.setContentText("Etiqueta del nodo:");
        return dialog;
    }

    private TextInputDialog makeSizeDialog() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Crear vertice");
        dialog.setContentText("Distancia entre nodo:");
        return dialog;
    }

    public void reiniciar() {
        this.idsNodos.clear();
        this.basePane.getChildren()
                     .clear();
    }

    public void setDesactivaVerticesObserver(Observable<Boolean> desactivaVerticesObs) {
        desactivaVerticesObs.subscribe(this.desactivaVerticesObs::onNext);
    }
}
