package mx.azkane.grafos.modelo;

import java.util.*;

public class Grafo {

    private Set<Vertice> vertices = new HashSet<>();

    public static HashMap<Vertice, CaminoCorto> calculaCaminoCorto(Grafo grafo, Vertice origen) {

        Set<Vertice> visitados = new HashSet<>();
        PriorityQueue<CaminoCorto> noVisitados = new PriorityQueue<>(grafo.vertices.size(),
                                                                     Comparator.comparingDouble(
                                                                             CaminoCorto::getDistancia));

        HashMap<Vertice, CaminoCorto> caminosCortos = new HashMap<>();

        CaminoCorto caminoCortoOrigen = new CaminoCorto(new LinkedList<>(), 0, origen);

        caminosCortos.put(origen, caminoCortoOrigen);
        noVisitados.add(caminoCortoOrigen);

        while (noVisitados.size() != 0) {
            CaminoCorto caminoCortoCurrent = noVisitados.poll();
            Vertice     currentNode        = caminoCortoCurrent.getVerticeDestino();
            if (!currentNode.isHabilitado()) continue;
            for (Map.Entry<Vertice, Double> adjacencyPair :
                    currentNode.getNodosAdyacentes()
                               .entrySet()) {
                Vertice nodoAdyacente = adjacencyPair.getKey();
                Double  ponderacion   = adjacencyPair.getValue();

                CaminoCorto caminoCortoHastaAdyacente =
                        caminosCortos.getOrDefault(nodoAdyacente,
                                                   new CaminoCorto(new LinkedList<>(),
                                                                   Integer.MAX_VALUE,
                                                                   nodoAdyacente));
                if (!visitados.contains(nodoAdyacente)) {
                    CaminoCorto nuevoCaminoCortoHastaAdyacente = calculaDistanciaMinima(caminoCortoHastaAdyacente,
                                                                                        ponderacion,
                                                                                        currentNode,
                                                                                        caminoCortoCurrent);
                    caminosCortos.put(nodoAdyacente, nuevoCaminoCortoHastaAdyacente);
                    noVisitados.add(nuevoCaminoCortoHastaAdyacente);
                }
            }
            visitados.add(currentNode);
        }
        return caminosCortos;
    }

    public static CaminoCorto distanciaMinimaDesdeOrigen(Grafo grafo, Vertice origen, Vertice destino) {
        if (grafo.vertices.isEmpty() || !origen.isHabilitado()) {
            return new CaminoCorto(new ArrayList<>(), 0, new Vertice(""));
        }

        HashMap<Vertice, CaminoCorto> caminosCortos = calculaCaminoCorto(grafo, origen);

        return caminosCortos.getOrDefault(destino, caminosCortos.get(origen));
    }

    private static CaminoCorto calculaDistanciaMinima(CaminoCorto caminoCortoHastaAdyacente,
                                                      Double ponderacion,
                                                      Vertice origen,
                                                      CaminoCorto caminoCortoDesdeOrigen) {
        double distanciaOrigen = caminoCortoDesdeOrigen.getDistancia();
        if (distanciaOrigen + ponderacion < caminoCortoHastaAdyacente.getDistancia()) {
            caminoCortoHastaAdyacente.setDistancia(distanciaOrigen + ponderacion);
            LinkedList<Vertice> shortestPath = new LinkedList<>(caminoCortoDesdeOrigen.getCaminoCorto());
            shortestPath.add(origen);
            caminoCortoHastaAdyacente.setCaminoCorto(shortestPath);
        }

        return caminoCortoHastaAdyacente;
    }

    public void addNodo(Vertice vertice) {
        vertices.add(vertice);
    }

    public void setVertices(Set<Vertice> vertices) {
        this.vertices = vertices;
    }
}
