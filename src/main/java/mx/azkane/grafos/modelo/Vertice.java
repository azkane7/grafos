package mx.azkane.grafos.modelo;

import java.util.*;

public class Vertice {
    private final Map<Vertice, Double> nodosAdyacentes       = new HashMap<>();
    private final Set<String>          nombreNodosAdyacentes = new HashSet<>();
    private String  nombre;
    private boolean habilitado = true;

    public Vertice(String nombre) {
        this.nombre = nombre;
    }

    public boolean esAdyacente(String nombreOtroNodo) {
        return nombreNodosAdyacentes.contains(nombreOtroNodo);
    }

    public void addArista(Vertice destino, double distancia) {
        nodosAdyacentes.put(destino, distancia);
        nombreNodosAdyacentes.add(destino.nombre);
    }

    public String getNombre() {
        return nombre;
    }

    public Map<Vertice, Double> getNodosAdyacentes() {
        return nodosAdyacentes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Vertice vertice = (Vertice) o;
        return Objects.equals(nombre, vertice.nombre);
    }

    @Override
    public int hashCode() {

        return Objects.hash(nombre);
    }

    @Override
    public String toString() {
        return "Vertice{" +
                "nombre='" + nombre + '\'' +
                ", nombreNodosAdyacentes=" + nombreNodosAdyacentes +
                ", habilitado=" + habilitado +
                "}";
    }

    public Vertice setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
        return this;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public Double getDistanciaToAdyacente(Vertice otro) {
        if (nodosAdyacentes.containsKey(otro)) {
            return nodosAdyacentes.get(otro);
        }

        return 0.0;
    }

    public void removeArista(Vertice otroVertice) {
        nombreNodosAdyacentes.remove(otroVertice.getNombre());
        nodosAdyacentes.remove(otroVertice);
    }
}
