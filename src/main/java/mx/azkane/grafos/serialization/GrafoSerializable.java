package mx.azkane.grafos.serialization;

import mx.azkane.grafos.controls.Nodo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@XmlRootElement
public class GrafoSerializable {
    private Set<NodoSerializable> nodos = new HashSet<>();
    private Set<EdgeSerializable> edges = new HashSet<>();

    public static GrafoSerializable of(List<Nodo> nodos) {
        GrafoSerializable grafoSerializable = new GrafoSerializable();
        for (Nodo nodo : nodos) {
            grafoSerializable.nodos.add(NodoSerializable.of(nodo));
            grafoSerializable.edges.addAll(EdgeSerializable.of(nodo));
        }

        return grafoSerializable;
    }

    public Set<NodoSerializable> getNodos() {
        return nodos;
    }

    @XmlElementWrapper(name = "nodos")
    @XmlElement(name = "nodo")
    public void setNodos(Set<NodoSerializable> nodos) {
        this.nodos = nodos;
    }

    public Set<EdgeSerializable> getEdges() {
        return edges;
    }

    @XmlElementWrapper(name = "edges")
    @XmlElement(name = "edge")
    public void setEdges(Set<EdgeSerializable> edges) {
        this.edges = edges;
    }

    @Override
    public String toString() {
        return "GrafoSerializable{" +
                "nodos=" + nodos +
                ", edges=" + edges +
                '}';
    }
}
