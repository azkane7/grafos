package mx.azkane.grafos.serialization;

import mx.azkane.grafos.controls.Nodo;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@XmlRootElement(namespace = "mx.azkane.grafos.serialization.GrafoSerializable")
public class NodoSerializable {
    private String nombre;
    private boolean habilitado;
    private double positionX;
    private double positionY;

    public static NodoSerializable of(Nodo nodo) {
        return new NodoSerializable()
                .setNombre(nodo.getNombre())
                .setPositionX(nodo.getLayoutX())
                .setPositionY(nodo.getLayoutY())
                ;
    }

    public String getNombre() {
        return nombre;
    }

    public NodoSerializable setNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public NodoSerializable setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
        return this;
    }

    public double getPositionX() {
        return positionX;
    }

    public NodoSerializable setPositionX(double positionX) {
        this.positionX = positionX;
        return this;
    }

    public double getPositionY() {
        return positionY;
    }

    public NodoSerializable setPositionY(double positionY) {
        this.positionY = positionY;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        NodoSerializable that = (NodoSerializable) o;
        return habilitado == that.habilitado &&
                Double.compare(that.positionX, positionX) == 0 &&
                Double.compare(that.positionY, positionY) == 0 &&
                Objects.equals(nombre, that.nombre);
    }

    @Override
    public int hashCode() {

        return Objects.hash(nombre, habilitado, positionX, positionY);
    }

    @Override
    public String toString() {
        return "NodoSerializable{" +
                "nombre='" + nombre + '\'' +
                ", habilitado=" + habilitado +
                ", positionX=" + positionX +
                ", positionY=" + positionY +
                '}';
    }
}
